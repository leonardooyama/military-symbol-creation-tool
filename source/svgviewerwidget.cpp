#include "svgviewerwidget.h"

SVGViewerWidget::SVGViewerWidget(QWidget *parent) : QWidget(parent)
{
    fileName.clear();
    svgData.clear();
}
void SVGViewerWidget::paintEvent(QPaintEvent *event)
{
    QPainter *painter = new QPainter();
    painter->begin(this);
    painter->save();
    QRect myRect =  event->rect();
    if (myRect.height() > myRect.width())
    {
        myRect.setHeight(myRect.width());
    }
    else
    {
        myRect.setWidth(myRect.height());
    }
    painter->setViewport(myRect);
    if(!svgData.isEmpty())
    {
        QSvgRenderer *renderer = new QSvgRenderer(svgData, this);
        renderer->render(painter);
    }
    else
    {
        if(!fileName.isEmpty())
        {
            QSvgRenderer *renderer = new QSvgRenderer(fileName, this);
            renderer->render(painter);
        }
    }
    painter->restore();
    painter->end();
}

