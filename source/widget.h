#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QJSEngine>

#include <QGraphicsSvgItem>
#include <QSvgRenderer>

#include <QPainter>
#include <QPaintDevice>
#include <QPaintEvent>

#include <QFile>
#include <QDebug>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Widget *ui;
    QJSEngine myEngine;
    bool isSVGfileCreated;
    bool isJavaScriptFileLoaded;
};

#endif // WIDGET_H
