#ifndef SVGVIEWERWIDGET_H
#define SVGVIEWERWIDGET_H

#include <QWidget>

#include <QPaintEvent>
#include <QPainter>
#include <QPaintDevice>

#include <QSvgRenderer>
#include <QDebug>


class SVGViewerWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SVGViewerWidget(QWidget *parent = nullptr);
    QString fileName;
    QByteArray svgData;
signals:
protected:
protected:
    void paintEvent(QPaintEvent *event) override;

public slots:
};

#endif // SVGVIEWERWIDGET_H
