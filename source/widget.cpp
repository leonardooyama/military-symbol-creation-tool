#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    isSVGfileCreated = false;
    ui->lineEdit->setText("SFP*S-----*****");
    isJavaScriptFileLoaded = false;
    QString fileName = ":/resources/milsymbol.js";
    QFile scriptFile(fileName);
    if (!scriptFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "could not open javascript file.";
    }
    else
    {
        QTextStream stream(&scriptFile);
        QString contents = stream.readAll();
        scriptFile.close();
        myEngine.evaluate(contents, fileName);
        isJavaScriptFileLoaded = true;
    }
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    if(isJavaScriptFileLoaded)
    {
        ui->widget->hide();
        QString script = "";
        script += "var renderedContent = \"\";";
        script += "renderedContent += new ms.Symbol('";
        script += ui->lineEdit->text();
        script += "', { size: 40, uniqueDesignation: \"ICON\", additionalInformation: '', infoFields: '' }).asSVG();";
        QJSValue result = myEngine.evaluate(script);
        if (result.isError())
        {
            qDebug() << "Uncaught exception at line" << result.property("lineNumber").toInt() << ":" << result.toString();
            return;
        }
        ui->widget->svgData = result.toString().toUtf8();
        // save svg file to hard disk
        QFile svgFile("./mySVG.svg");
        if (!svgFile.open(QIODevice::ReadWrite))
        {
            qDebug() << "could not open file";
            return;
        }
        else
        {
            QTextStream out(&svgFile);
            out << result.toString();
            svgFile.close();
            isSVGfileCreated = true;
        }
        ui->widget->show();
    }
}
