# Military Symbol Creation Tool

Qt C++ tool to demonstrate how to generate military symbols using Milsymbol, which is a small library in pure javascript that creates military unit symbols according to MIL-STD-2525 and STANAG APP-6. This project uses code from https://github.com/spatialillusions/milsymbol.

Special thanks to Måns Beckman (mans.beckman@spatialillusions.com).
